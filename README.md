# jfuentesibanez/datasets

Datasets for different projects..

## Requirements

- none

## Installation

Clone repository:
```bash
$ git clone https://gitlab.com/jfuentesibanez/datasets.git && cd datasets
```

## Usage

Use the datasets directly from Jupyter Notebooks
```bash
url = 'copied_raw_dataset_link'
df1 = pd.read_csv(url)
# Dataset is now stored in a Pandas Dataframe
```

## License

Akoios Titan (c) 2019 - All rights reserved.
